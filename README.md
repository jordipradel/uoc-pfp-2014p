# README #

### What is this repository for? ###

* This is part of a wording for a software engineering project at UOC.

### How to download the code ###

There are 2 options:

* Just clone the repository if you know how to use GIT
* Download the source code (by clicking the download icon, last one on your left)
