package edu.uoc.pfp.lists.test;

import edu.uoc.pfp.lists.app.*;
import edu.uoc.pfp.lists.dal.InMemoryBoardsRepository;
import edu.uoc.pfp.lists.domain.*;
import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListsServiceTest {

    @NotNull
    private ListsService listsService;

    @NotNull
    private Id sampleBoardId;

    private Collaborator jane = new Collaborator("jane@example.com");

    @Before
    public void init(){
        BoardsRepository boardsRepository = new InMemoryBoardsRepository();
        listsService = new ListsServiceImpl(boardsRepository);
        sampleBoardId = listsService.newBoard("Board 1", BoardVisibility.privateBoard, jane);
    }

    @Test
    public void testNewBoard(){
        Id id = listsService.newBoard("Sample board", BoardVisibility.privateBoard, jane);
        Board board = listsService.getBoard(id);
        assertEquals("Sample board", board.getName());
        assertEquals(BoardVisibility.privateBoard,board.getVisibility());
        assertEquals(3,board.getLists().size());
        assertEquals("To Do", board.getLists().get(0).getName());
        assertEquals(Utils.<Card>list(), board.getLists().get(0).getCards());
        assertEquals("Doing", board.getLists().get(1).getName());
        assertEquals(Utils.<Card>list(), board.getLists().get(1).getCards());
        assertEquals("Done", board.getLists().get(2).getName());
        assertEquals(Utils.<Card>list(), board.getLists().get(2).getCards());
    }

    @Test
    public void testRenameBoard(){
        listsService.renameBoard(sampleBoardId, "New name");
        Board board = listsService.getBoard(sampleBoardId);
        assertEquals("New name", board.getName());
    }

    @Test
    public void testAddList(){
        listsService.addList(sampleBoardId, "New list");
        Board board = listsService.getBoard(sampleBoardId);
        assertEquals(4,board.getLists().size());
        assertEquals("New list", board.getLists().get(3).getName());
        assertEquals(Utils.<Card>list(), board.getLists().get(3).getCards());
    }

    @Test
    public void testRenameList(){
        listsService.renameList(sampleBoardId, "To Do", "Backlog");
        Board board = listsService.getBoard(sampleBoardId);
        assertEquals("Backlog",board.getLists().get(0).getName());
    }

    @Test
    public void testAddCardToList(){
        listsService.addCard(sampleBoardId,"To Do","Sample card");
        List<Card> toDoCards = listsService.getBoard(sampleBoardId).getLists().get(0).getCards();
        assertEquals(1,toDoCards.size());
        Card newCard = toDoCards.get(0);
        assertEquals("Sample card",newCard.getName());
        assertEquals(Utils.<Color>set(),newCard.getColors());
    }

    @Test
    public void testEditCardDescription(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.editCardDescription(sampleBoardId, sampleCardId, "New description");
        Card sampleCard = listsService.getCard(sampleBoardId, sampleCardId);
        assertEquals("New description", sampleCard.getDescription());
    }

    @Test
    public void testMoveCard(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.moveCard(sampleBoardId,sampleCardId,"Doing",1);
        List<Card> toDoCards = listsService.getBoard(sampleBoardId).getLists().get(0).getCards();
        assertEquals(0, toDoCards.size());
        List<Card> doneCards = listsService.getBoard(sampleBoardId).getLists().get(1).getCards();
        assertEquals(1, doneCards.size());
        assertEquals(sampleCardId, doneCards.get(0).getId());
    }

    @Test
    public void tesAddCardColor(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.addCardColor(sampleBoardId, sampleCardId, Color.green);
        Card greenCard = listsService.getBoard(sampleBoardId).getLists().get(0).getCards().get(0);
        assertEquals(Utils.set(Color.green), greenCard.getColors());
    }

    @Test
    public void testAddChecklist(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.addChecklist(sampleBoardId,sampleCardId,"Tasks");
        Card cardDetails = listsService.getCard(sampleBoardId, sampleCardId);
        Checklist checklist = cardDetails.getChecklists().get(0);
        assertEquals("Tasks", checklist.getName());
        assertEquals(0, checklist.getTasks().size());
    }

    @Test
    public void testAddTask(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id sampleChecklistId = listsService.addChecklist(sampleBoardId, sampleCardId, "Tasks");
        listsService.addTask(sampleBoardId,sampleChecklistId,"Sample task");
        Card cardDetails = listsService.getCard(sampleBoardId, sampleCardId);
        Checklist checklist = cardDetails.getChecklists().get(0);
        assertEquals(1,checklist.getTasks().size());
        Task task = checklist.getTasks().get(0);
        assertEquals("Sample task",task.getName());
        assertEquals(false,task.isDone());
    }

    @Test
    public void testMarkTaskDone(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id sampleChecklistId = listsService.addChecklist(sampleBoardId, sampleCardId, "Tasks");
        Id sampleTaskId = listsService.addTask(sampleBoardId,sampleChecklistId,"a");
        listsService.markTaskDone(sampleBoardId, sampleTaskId);
        Card card = listsService.getCard(sampleBoardId, sampleCardId);
        Task task = card.getChecklists().get(0).getTasks().get(0);
        assertEquals(true, task.isDone());
    }

    @Test
    public void testTasksCounters(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id checklist1 = listsService.addChecklist(sampleBoardId,sampleCardId,"Tasks");
        Id checklist2 = listsService.addChecklist(sampleBoardId,sampleCardId,"Impediments");
        Id taskAId = listsService.addTask(sampleBoardId,checklist1,"a");
        listsService.addTask(sampleBoardId,checklist1,"b");
        listsService.addTask(sampleBoardId,checklist1,"c");
        Id taskBId = listsService.addTask(sampleBoardId,checklist2,"1");
        listsService.addTask(sampleBoardId,checklist2,"2");
        listsService.markTaskDone(sampleBoardId, taskAId);
        listsService.markTaskDone(sampleBoardId, taskBId);
        Card sampleCard = listsService.getBoard(sampleBoardId).getLists().get(0).getCards().get(0);
        TaskCompletion tc = sampleCard.getTaskCompletion();
        assertEquals(2, tc.completedTasks);
        assertEquals(5, tc.totalNumberOfTasks);

    }


}
