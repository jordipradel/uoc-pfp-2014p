package edu.uoc.pfp.lists.test;

import edu.uoc.pfp.lists.app.ListsService;
import edu.uoc.pfp.lists.dal.InMemoryBoardsRepository;
import edu.uoc.pfp.lists.domain.*;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CollaboratorsTest {

    private Collaborator jane = new Collaborator("jane@example.com");
    private Collaborator john = new Collaborator("john@exameple.com");

    @NotNull
    private ListsService listsService;

    @NotNull
    private Id b;

    private Id featureA;
    private Id featureB;
    private Id featureATests;
    private Id featureACode;
    private Id featureBTests;
    private Id featureBCode;

    @Before
    public void init() {
        BoardsRepository boardsRepository = new InMemoryBoardsRepository();
        listsService = new ListsServiceImpl(boardsRepository);
        b = listsService.newBoard("Board 1", BoardVisibility.privateBoard, jane);
        listsService.addCollaborator(b, john);
        listsService.addList(b, "TO-DO");
        featureA = listsService.addCard(b, "TO-DO", "Feature A");
        Id featureATasks = listsService.addChecklist(b, featureA, "Tasks");
        featureB = listsService.addCard(b, "TO-DO", "Feature B");
        Id featureBTasks = listsService.addChecklist(b, featureB, "Tasks");
        featureATests = listsService.addTask(b, featureATasks, "Implement A tests");
        featureACode = listsService.addTask(b, featureATasks, "Implement A code");
        featureBTests = listsService.addTask(b, featureBTasks, "Implement B tests");
        featureBCode = listsService.addTask(b, featureBTasks, "Implement B code");
    }

    @Test
    public void testAddCollaborator() {
        Collaborator sun = new Collaborator("sun@example.com");
        listsService.addCollaborator(b, sun);
        java.util.List<Collaborator> collaborators = listsService.getCollaborators(b);
        assertEquals(3, collaborators.size());
        assert (collaborators.get(2).equals(sun));
    }

    @Test
    public void testInitialTaskCollaborators(){
        Card c = listsService.getCard(b,featureA);
        assert(c.getCollaborators().size() == 0);
    }

    @Test
    public void testAddInexistingCollaborator(){
        try {
            listsService.addCollaboratorToTask(b, featureA, new Collaborator("vega@example.com"));
            fail("Should have thrown an exception");
        }catch(IllegalArgumentException e){
            assert(e.getMessage().equals("vega@example.com is not a collaborator fo the board"));
        }
    }

    @Test
    public void testAddCollaboratorToTask() {
        listsService.addCollaboratorToTask(b, featureA, jane);
        Card c = listsService.getCard(b,featureA);
        java.util.List<Collaborator> featureACollaborators = c.getCollaborators();
        assert(featureACollaborators.size() == 1);
        assert(featureACollaborators.get(0) == jane);
    }

    @Test
    public void testGetCollaboratorTasks(){
        listsService.addCollaboratorToTask(b, featureA, jane);
        listsService.addCollaboratorToTask(b, featureA, john);
        listsService.addCollaboratorToTask(b, featureB, jane);
        java.util.List<Task> janeTasks = listsService.getCollaboratorTasks(b, jane);
        assert(janeTasks.size() == 4);
        assert(janeTasks.get(0).getName().equals("Implement A tests"));
        assert(janeTasks.get(1).getName().equals("Implement A code"));
        assert(janeTasks.get(2).getName().equals("Implement B tests"));
        assert(janeTasks.get(3).getName().equals("Implement B code"));
    }


}
