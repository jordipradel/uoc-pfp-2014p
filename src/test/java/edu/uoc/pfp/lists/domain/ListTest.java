package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.lists.domain.*;
import edu.uoc.pfp.util.Utils;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

import static edu.uoc.pfp.util.Utils.*;

public class ListTest extends TestCase {

    @NotNull
    Board b = new Board("SampleBoard", BoardVisibility.privateBoard);

    public void testRename(){
        List l = b.addList("SampleList");
        l.rename("newName");
        assertEquals("newName",l.getName());
    }

    public void testAddCard(){
        List l = b.addList("SampleList");
        Card c = l.addCard(new Id("1"),"SampleCard");
        assertEquals("SampleCard",c.getName());
        assertEquals(list(c), l.getCards());
        assertEquals(1,c.getPosition());
        assertEquals(Utils.<Label>set(),c.getLabels());
    }
}
