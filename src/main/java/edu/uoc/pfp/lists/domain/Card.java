package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static edu.uoc.pfp.util.Utils.requireArg;

public class Card {

    @NotNull
    private List list;

    @NotNull
    private Id id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Set<Label> labels = new HashSet<Label>();

    @NotNull
    private java.util.List<Checklist> checklists = new LinkedList<Checklist>();

    @NotNull
    private java.util.List<Collaborator> collaborators = new LinkedList<Collaborator>();

    Card(@NotNull List list, @NotNull Id id, @NotNull String name) {
        this.id = id;
        this.list = list;
        this.name = name;
    }

    void editDescription(@NotNull String newDescription) {
        requireArg(newDescription.length() > 0, "Card description can't be empty but not null");
        this.description = newDescription;
    }

    void move(@NotNull List l, int position) {
        if (list.getId().equals(l.getId())) {
            list.moveCard(this, position);
        } else {
            this.list.removeCard(this);
            l.addCard(this, position);
            this.list = l;
        }
    }

    void addLabel(@NotNull Label l) {
        boolean added = this.labels.add(l);
        requireArg(added, "Label already set");
    }

    public Set<Color> getColors() {
        Set<Color> colors = new HashSet<Color>();
        for (Label l : getLabels()) {
            colors.add(l.getColor());
        }
        return colors;
    }

    public Id addChecklist(@NotNull String checklistName) {
        Id newChecklistId = new Id();
        Checklist c = new Checklist(newChecklistId, checklistName);
        checklists.add(c);
        return newChecklistId;
    }

    private java.util.List<Task> getAllTasks(){
        java.util.List<Task> result = new LinkedList<Task>();
        for(Checklist c : checklists){
            result.addAll(c.getTasks());
        }
        return result;
    }

    public TaskCompletion getTaskCompletion() {
        int completedTasks = 0;
        java.util.List<Task> allTasks = getAllTasks();
        int totalNumberOfTasks = allTasks.size();
        for(Task t : allTasks){
            if(t.isDone()) completedTasks++;
        }
        return new TaskCompletion(completedTasks,totalNumberOfTasks);
    }


    @NotNull
    public Id getId() {
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPosition() {
        return list.getCardPosition(this);
    }

    @NotNull
    public Set<Label> getLabels() {
        return labels;
    }

    @NotNull
    public java.util.List<Checklist> getChecklists() {
        return checklists;
    }

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                '}';
    }

    @NotNull
    public java.util.List<Collaborator> getCollaborators() {
        return collaborators;
    }
}
