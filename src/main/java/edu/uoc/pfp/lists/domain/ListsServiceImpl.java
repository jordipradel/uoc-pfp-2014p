package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.lists.app.ListsService;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListsServiceImpl implements ListsService {


    @NotNull
    private BoardsRepository boardsRepository;

    public ListsServiceImpl(@NotNull BoardsRepository boardsRepository) {
        this.boardsRepository = boardsRepository;
    }


    @Override
    public Id newBoard(@NotNull String boardName, @NotNull BoardVisibility boardVisibility, @NotNull Collaborator creator) {
        Board b = new Board(boardName,boardVisibility);
        b.addList("To Do");
        b.addList("Doing");
        b.addList("Done");
        b.addCollaborator(creator);
        boardsRepository.save(b);
        return b.getId();
    }

    @Override
    public Board getBoard(@NotNull Id id) {
        return boardsRepository.getBoard(id);
    }

    @Override
    public void renameBoard(@NotNull Id id, @NotNull String newName) {
        Board b = boardsRepository.getBoard(id);
        b.rename(newName);
        boardsRepository.save(b);
    }

    @Override
    public void addList(@NotNull Id boardId, @NotNull String listName) {
        Board b = boardsRepository.getBoard(boardId);
        b.addList(listName);
    }

    @Override
    public void renameList(@NotNull Id boardId, @NotNull String currentListName, @NotNull String newListName) {
        Board b = boardsRepository.getBoard(boardId);
        b.renameList(currentListName,newListName);
    }

    @Override
    public Id addCard(@NotNull Id boardId, @NotNull String listName, @NotNull String cardName) {
        Board b = boardsRepository.getBoard(boardId);
        return b.addCard(listName, cardName);
    }

    @Override
    public void addCardColor(Id boardId, Id cardId, Color color) {
        Board b = boardsRepository.getBoard(boardId);
        b.addCardColor(cardId, color);
    }

    @Override
    public void editCardDescription(@NotNull Id boardId, @NotNull Id cardId, @NotNull String newDescription) {
        Board b = boardsRepository.getBoard(boardId);
        b.editCardDescription(cardId,newDescription);
    }

    @Override
    public Card getCard(@NotNull Id boardId, @NotNull Id cardId) {
        Board b = boardsRepository.getBoard(boardId);
        return b.getCard(cardId);
    }

    @Override
    public void moveCard(@NotNull Id boardId, @NotNull Id cardId, @NotNull String destinationListName, int position) {
        Board b = boardsRepository.getBoard(boardId);
        b.moveCard(cardId, destinationListName, position);
    }

    @Override
    public Id addChecklist(@NotNull Id boardId, @NotNull Id cardId, @NotNull String checklistName) {
        Board b = boardsRepository.getBoard(boardId);
        return b.addChecklist(cardId,checklistName);
    }

    @Override
    public Id addTask(@NotNull Id boardId, @NotNull Id checklistId, @NotNull String taskName) {
        Board b = boardsRepository.getBoard(boardId);
        return b.addTask(checklistId,taskName);
    }

    @Override
    public void markTaskDone(@NotNull Id boardId, @NotNull Id taskId) {
        Board b = boardsRepository.getBoard(boardId);
        b.markTaskDone(taskId);

    }

    @Override
    public void addCollaborator(@NotNull Id boardId, @NotNull Collaborator collaborator) {
        Board b = boardsRepository.getBoard(boardId);
        b.addCollaborator(collaborator);
    }

    @Override
    public List<Collaborator> getCollaborators(Id boardId) {
        Board b = boardsRepository.getBoard(boardId);
        return b.getCollaborators();
    }

    @Override
    public void addCollaboratorToTask(Id boardId, Id taskId, Collaborator collaborator) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public List<Task> getCollaboratorTasks(Id boardId, Collaborator collaborator) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
