package edu.uoc.pfp.lists.domain;


import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.List;

public class Checklist {

    @NotNull private Id id;

    @NotNull private String name;

    @NotNull private java.util.List<Task> tasks = new LinkedList<Task>();

    public Checklist(@NotNull Id id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    @NotNull
    public Id getId() {
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public List<Task> getTasks() {
        return tasks;
    }

    public Id addTask(String taskName) {
        Id id = new Id();
        Task task = new Task(id, taskName);
        tasks.add(task);
        return id;
    }
}
