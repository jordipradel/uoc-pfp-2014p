package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Class representing boards
 */
public class Board {

    @NotNull
    private Id id;

    @NotNull
    private String name;

    @NotNull
    private BoardVisibility visibility;

    @NotNull
    private java.util.List<List> lists = new LinkedList<List>();

    @NotNull
    private java.util.List<Label> labels;

    @NotNull
    private java.util.List<Collaborator> collaborators = new LinkedList<Collaborator>();

    Board(@NotNull String name, @NotNull BoardVisibility visibility) {
        this.id = new Id();
        this.name = name;
        this.visibility = visibility;
        this.labels = Label.createLabelSet();
    }

    @NotNull
    List addList(@NotNull String listName) {
        List l = new List(new Id(), listName);
        lists.add(l);
        return l;
    }

    void rename(@NotNull String newName) {
        if (newName.length() == 0) throw new IllegalArgumentException("Board name too short");
        this.name = newName;
    }

    @NotNull
    Id addCard(@NotNull String listName, @NotNull String cardName) {
        Id newCardId = new Id();
        List list = getList(listName);
        list.addCard(newCardId, cardName);
        return newCardId;
    }

    void renameList(String currentListName, String newListName) {
        List list = getList(currentListName);
        list.rename(newListName);
    }

    void addCardColor(Id cardId, Color color) {
        Card card = getCard(cardId);
        Label label = getLabel(color);
        card.addLabel(label);
    }

    void editCardDescription(Id cardId, String newDescription) {
        Card card = getCard(cardId);
        card.editDescription(newDescription);
    }

    void moveCard(Id cardId, String destinationListName, int position) {
        Card card = getCard(cardId);
        List l = getList(destinationListName);
        card.move(l, position);
    }

    public Id addChecklist(@NotNull Id cardId, @NotNull String checklistName) {
        Card c = getCard(cardId);
        return c.addChecklist(checklistName);
    }

    public Id addTask(@NotNull Id checklistId, @NotNull String taskName) {
        Checklist c = getChecklist(checklistId);
        return c.addTask(taskName);
    }

    public void markTaskDone(Id taskId) {
        Task t = getTask(taskId);
        t.markDone();
    }

    public void addCollaborator(Collaborator collaborator) {
        if(!collaborators.contains(collaborator)) {
            collaborators.add(collaborator);
        }
    }

    @NotNull
    private Label getLabel(Color color) {
        for (Label l : labels) {
            if (l.getColor() == color) {
                return l;
            }
        }
        throw new IllegalStateException("This board has no label for color " + color);
    }


    @NotNull
    private List getList(String listName) {
        for (List l : lists) {
            if (l.getName().equals(listName)) return l;
        }
        throw new IllegalArgumentException("List " + listName + " not found in board " + this.name);
    }

    @NotNull
    private java.util.List<Card> getAllCards() {
        java.util.List<Card> result = new LinkedList<Card>();
        for (List l : lists) {
            for (Card c : l.getCards()) {
                result.add(c);
            }
        }
        return result;
    }

    @NotNull
    public Card getCard(Id cardId) {
        for (Card c : getAllCards()) {
            if (c.getId().equals(cardId)) return c;
        }
        throw new IllegalArgumentException("Card " + cardId + " not found in board " + this.name);
    }

    @NotNull
    private Checklist getChecklist(@NotNull Id checklistId) {
        for (Card c: getAllCards()) {
            for(Checklist cl : c.getChecklists()){
                if(cl.getId().equals(checklistId)){
                    return cl;
                }
            }
        }
        throw new IllegalArgumentException("Checklist " + checklistId + " not found in board " + this.name);
    }

    @NotNull
    private Task getTask(@NotNull Id taskId){
        for(Card c: getAllCards()){
            for(Checklist cl : c.getChecklists()){
                for(Task t: cl.getTasks()){
                    if(taskId.equals(t.getId())){
                        return t;
                    }
                }
            }
        }
        throw new IllegalArgumentException("Task " + taskId + " not found in board " + this.name);

    }

    @NotNull
    public Id getId() {
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public BoardVisibility getVisibility() {
        return visibility;
    }

    @NotNull
    public java.util.List<List> getLists() {
        return Collections.unmodifiableList(lists);
    }

    @NotNull
    public java.util.List<Label> getLabels() {
        return Collections.unmodifiableList(labels);
    }

    @NotNull
    public java.util.List<Collaborator> getCollaborators() {
        return collaborators;
    }
}
