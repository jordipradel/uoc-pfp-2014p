package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.LinkedList;

import static edu.uoc.pfp.util.Utils.*;

/** Class representing lists, each one in a board */
public class List {

    @NotNull
    private Id id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private java.util.List<Card> cards = new LinkedList<Card>();

    List(@NotNull Id id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    @NotNull
    Card addCard(@NotNull Id newCardId, @NotNull String cardName){
        Card c = new Card(this,newCardId,cardName);
        cards.add(c);
        return c;
    }

    void rename(@NotNull String newName){
        if(newName.length() == 0) throw new IllegalArgumentException("List name too short");
        this.name = newName;
    }

    void addCard(Card card, int position) {
        requireArg(position > 0, "First position is 1");
        requireArg(position <= cards.size() + 1, "Can't add a card beyond last position");
        this.cards.add(position-1,card);
    }

    void moveCard(Card card, int newPosition) {
        int currentPosition = getCardPosition(card);
        requireArg(currentPosition > 0, "Can't move a card that doesn't belog to this list");
        cards.remove(currentPosition-1);
        if(currentPosition < newPosition){
            cards.add(newPosition-1,card);
        } else {
            cards.add(newPosition-1,card);
        }
    }

    void removeCard(Card card) {
        boolean removed = cards.remove(card);
        requireArg(removed,"Card " + card + " not found in list " + this.getName());
    }


    int getCardPosition(Card card){
        return cards.indexOf(card) + 1;
    }

    @NotNull
    Id getId(){
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public java.util.List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    @Override
    public String toString() {
        return "List{" +
                "name='" + name + '\'' +
                '}';
    }

}
