package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

public interface BoardsRepository {

    void save(@NotNull Board board);

    @NotNull
    Board getBoard(@NotNull Id id);
}
