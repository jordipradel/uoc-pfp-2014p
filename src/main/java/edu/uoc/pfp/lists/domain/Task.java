package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;

public class Task {

    @NotNull private Id id;

    @NotNull private String name;

    private boolean done = false;

    @NotNull
    private java.util.Set<Collaborator> collaborators = new HashSet<Collaborator>();


    public Task(@NotNull Id id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    public void markDone(){
        Utils.requireArg(!this.done,"The task " + id + " is already done");
        this.done = true;
    }

    @NotNull
    public Id getId() {
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public boolean isDone() {
        return done;
    }
}
