package edu.uoc.pfp.lists.domain;


import org.jetbrains.annotations.NotNull;

public class Collaborator {

    @NotNull
    private String email;

    public Collaborator(@NotNull String email) {
        this.email = email;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collaborator that = (Collaborator) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}
