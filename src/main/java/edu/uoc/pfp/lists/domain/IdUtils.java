package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public final class IdUtils {

    @NotNull
    public static String newId(){
        return UUID.randomUUID().toString();
    }

}
