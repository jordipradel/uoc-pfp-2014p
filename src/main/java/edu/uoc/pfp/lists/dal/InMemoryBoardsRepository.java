package edu.uoc.pfp.lists.dal;

import edu.uoc.pfp.lists.domain.Board;
import edu.uoc.pfp.lists.domain.BoardsRepository;
import edu.uoc.pfp.lists.domain.Id;
import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class InMemoryBoardsRepository implements BoardsRepository {

    @NotNull
    private Map<Id,Board> boardsById = new HashMap<Id, Board>();

    @Override
    public void save(Board board) {
        boardsById.put(board.getId(),board);
    }

    @Override @NotNull
    public Board getBoard(@NotNull Id id) {
        Board result = boardsById.get(id);
        Utils.requireArg(result != null, "Board " + id + " does not exist");
        return result;
    }
}
