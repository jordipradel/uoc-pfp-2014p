package edu.uoc.pfp.lists.app;

import edu.uoc.pfp.lists.domain.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ListsService {

    Id newBoard(@NotNull String boardName, @NotNull BoardVisibility boardVisibility, @NotNull Collaborator creator);

    Board getBoard(Id id);

    void renameBoard(Id id, String newName);

    void addList(Id boardId, String listName);

    void renameList(Id boardId, String currentListName, String newListName);

    Id addCard(Id boardId, String listName, String cardName);

    void addCardColor(Id boardId, Id cardId, Color color);

    void editCardDescription(Id boardId, Id cardId, String newDescription);

    Card getCard(Id boardId, Id cardId);

    void moveCard(Id boardId, Id cardId, String destinationListName, int position);

    /**
     * Creates a new checklist in the card identified by cardId in the board identified by boardId
     * @param boardId The board id
     * @param cardId The card id of the card where the checklist is to be created
     * @param checklistName The name of the new checklist
     * @return The id of the newly created checklist
     */
    Id addChecklist(Id boardId, Id cardId, String checklistName);

    /**
     * Creates a new task in the checklist identified by checklistId in the board identified by boardId
     * @param boardId The board id
     * @param checklistId The checklist id of the checklist where the task is to be created
     * @param taskName The name of the new task
     * @return The id of the newly creted task
     */
    Id addTask(Id boardId, Id checklistId, String taskName);

    /**
     * Marks the task identified by taskId in the board identified by boardId as done
     * @throws java.lang.IllegalArgumentException if the board or the task don't exist or if the task is already done
     * @param boardId The board id
     * @param taskId The task id of the task to be marked as done
     */
    void markTaskDone(Id boardId, Id taskId);

    /**
     * Adds a collaborator to a board
     * @param boardId The board id
     * @param collaborator The collaborator to add
     */
    void addCollaborator(Id boardId, Collaborator collaborator);

    /**
     * Returns the collaborators of a board
     * @param boardId The board id
     * @return The list of collaborators of the board boardId
     */
    List<Collaborator> getCollaborators(Id boardId);

    void addCollaboratorToTask(Id boardId, Id taskId, Collaborator collaborator);

    List<Task> getCollaboratorTasks(Id boardId, Collaborator collaborator);
}
