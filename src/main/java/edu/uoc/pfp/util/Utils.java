package edu.uoc.pfp.util;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Utils {

    @NotNull
    public static <T> List<T> list(T... args){
        return Arrays.asList(args);
    }

    @NotNull
    public static <T> Set<T> set(T... args){
        return new HashSet<T>(Arrays.asList(args));
    }

    public static void requireArg(boolean condition, @NotNull String requirement){
        if(!condition) throw new IllegalArgumentException(requirement);
    }

}
